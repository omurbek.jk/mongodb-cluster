#!/bin/bash
set -x

source /etc/profile.d/cluster

if [ "$NODE_TYPE" != "Secondary" ]; then
    exit 0
fi

## we need to give some time to quiet down
sleep 20

containsElement () {
  local e
  for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 0; done
  return 1
}

#################################################################
# Configure the replica sets, set this host as Primary with
# highest priority
#################################################################
port=27017
votes=1
read -a existingIds <<< "$(./orchestrate-non-dynamo.sh -i "${PRIMARY_ADDRESS}:${port}")"
highest_priority="$(./orchestrate-non-dynamo.sh -r "${PRIMARY_ADDRESS}:${port}")"

sortedAddresses="$(echo "$CLUSTER_ADDRESSES, $MY_IPADDRESS" | tr , "\n" | sort)"
location=0
while read addr; do
    if [ "$MY_IPADDRESS" = "${addr}" ]; then
        break
    fi
    location=$((location + 1))
done <<< "$(echo "$sortedAddresses")"
echo $location
seqLocation=0
newId=0
while true; do
    containsElement "$newId" "${existingIds[@]}"
    if [ $? -eq 0 ]; then
        ## we can't use this as an index
	newId=$((newId + 1))
        continue
    fi
    ## if we are here, we can use it, but the sequence location must be equal to our location
    if [ "$seqLocation" == "$location" ]; then
        ## after we increment
	break
    fi
    newId=$((newId + 1))
    seqLocation=$((seqLocation + 1))
done

## add myself in, but don't allow voting or the ability to take over primary initially
conf="{_id : ${newId}, host: '${MY_IPADDRESS}:${port}', priority: 0, votes: 0}"
mongo --host ${PRIMARY_ADDRESS} --port ${port} <<EOF
rs.add(${conf})
EOF

## write data to master to keep the failover election from happening
## might as well record ourselves in the DB anyway
mongo --host ${PRIMARY_ADDRESS} --port ${port} <<EOF
use replicas;
db.hosts.insert({ip: '${MY_IPADDRESS}', port: ${port}})
EOF

## dissallow becoming master for 5 minutes on this host
mongo <<EOF
rs.freeze(300)
EOF

## now that we are frozen, lets take a breath and then re-add our priority and voting ability back
sleep 10

## priority should be the lowest possible to ensure we do not get elected
priority="$((highest_priority + newId))"
myIndex="$(./orchestrate-non-dynamo.sh -x "${MY_IPADDRESS}:${port}")"
mongo --host ${PRIMARY_ADDRESS} --port ${port} <<EOF
cfg = rs.conf()
cfg.members[$myIndex].priority = $priority
cfg.members[$myIndex].votes = $votes
rs.reconfig(cfg)
EOF

if [ $? -ne 0 ]; then
    # a problem occured
    echo "error"
    exit 1
fi

