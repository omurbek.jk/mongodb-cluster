#!/bin/bash

/bin/dd if=/dev/zero of=/data/swap.1 bs=1M count=4096
/sbin/mkswap /data/swap.1
chmod 600 /data/swap.1
/sbin/swapon /data/swap.1

if ! cat /etc/fstab | grep -q 'swap.1'; then
    echo "/data/swap.1 swap swap defaults 0 0" >> /etc/fstab
    echo >> /etc/fstab
fi
