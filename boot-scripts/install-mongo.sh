#!/bin/bash
## orriginally from https://raw.githubusercontent.com/aws-quickstart/quickstart-mongodb/develop/scripts/init.sh
set -x

source lib/helpers.sh


(
    workdir="$PWD"
    cd /home/ec2-user/mongodb
    
    #################################################################
    # Lets source in our /etc/profile so we know if we are the
    # primary or not
    #################################################################
    source /etc/profile.d/cluster
    if [ "$IS_LEADER" = "true" ]; then
        NODE_TYPE="Primary"
    else
        NODE_TYPE="Secondary"
    fi
    echo "export NODE_TYPE='$NODE_TYPE'" >> /etc/profile.d/cluster
    (
	addresses="$(python ${workdir}/../extends/boot-scripts/lib/group_addresses.py)"
	LEADER_IP="$(echo $addresses | perl -pe 's{\s}{\n}g' | head -1)"
    )
    # MongoDBVersion set inside config.yaml
    version=${MONGO_VERSION:-"3.2"}
    
    if [ "${version}" == "2.6" ]; then
        echo "[mongodb-org-${version}]
name=MongoDB 2.6 Repository
baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64/
gpgcheck=0
enabled=1" > /etc/yum.repos.d/mongodb-org-${version}.repo
    else
        echo "[mongodb-org-${version}]
name=MongoDB Repository
baseurl=http://repo.mongodb.org/yum/amazon/2013.03/mongodb-org/${version}/x86_64/
gpgcheck=0
enabled=1" > /etc/yum.repos.d/mongodb-org-${version}.repo
    fi
    
    # To be safe, wait a bit for flush
    sleep 5
    
    yum --enablerepo=epel install node npm -y
    yum install -y mongodb-org munin-node libcgroup mongo-10gen-server mongodb-org-shell sysstat
    
    #################################################################
    #  Figure out what kind of node we are and set some values
    #################################################################
    NODES="$((${NUMBER_OF_CLUSTER_NODES} - 1))"
    MICROSHARDS="${MICROSHARDS:-0}"
    SHARDCOUNT="${SHARD_COUNT}"
    
    #################################################################
    #  When there is no sharding, it's a replica set
    #################################################################
    
    if [ "${SHARDCOUNT}" == "0" ]; then
        MICROSHARDS=0
    fi
    
    #################################################################
    #  When there is sharding, make sure atleast one microshard
    #################################################################
    
    if [ "${SHARDCOUNT}" != "0" ]; then
        if [ "${MICROSHARDS}" == "0" ]; then
            MICROSHARDS=1
        fi
    fi
    
    #  Do NOT use timestamps here!!
    # This has to be unique across multiple runs!
    UNIQUE_NAME=MONGODB_${CLUSTER_NAME}_${VPC_NAME}
    port=27017

    #################################################################
    #  Go find a primary node if we are not the leader
    #################################################################
    if [ "${NODE_TYPE}" != "Primary" ]; then
	## make IPADDRS all our siblings (this server included)
	while true; do
	    addrToCheck=""
	    addr=""
	    ## get the current list of addrs in the autoscale group
	    addresses="$(python ${workdir}/../extends/boot-scripts/lib/group_addresses.py)"
	    read -a IPADDRS <<< $addresses
	    
	    ## pick one we know isn't ready yet
	    for addr in "${IPADDRS[@]}"; do
		## clean up the ip address a bit
		addr="${addr%\"}"
		addr="${addr#\"}"
		
		## lets not worry about ourself since we are not primary
		if [ "$MY_IPADDRESS" = "$addr" ]; then
		    continue;
		fi
		primaryState="$(./orchestrate-non-dynamo.sh -p ${addr}:${port})"
		if [ "$primaryState" == "0" ]; then
		    ## we found our primary
		    PRIMARY_ADDRESS="${addr}"
		    echo "export PRIMARY_ADDRESS='${PRIMARY_ADDRESS}'" >> /etc/profile.d/cluster
		fi
	    done
	    if [ -z "${PRIMARY_ADDRESS:-}" ]; then
		sleep 5
	    else
		break
	    fi
	done
        read -a IPADDRS <<< $addresses
    fi
    ## now we are ready to install mongodb
    #################################################################
    # Setup MongoDB servers and config nodes
    #################################################################
    mkdir /var/run/mongod
    chown mongod:mongod /var/run/mongod
    
    echo "net:" > mongod.conf
    echo "  port:" >> mongod.conf
    echo "" >> mongod.conf
    echo "systemLog:" >> mongod.conf
    echo "  destination: file" >> mongod.conf
    echo "  logAppend: true" >> mongod.conf
    echo "  path: /log/mongod.log" >> mongod.conf
    echo "" >> mongod.conf
    echo "storage:" >> mongod.conf
    echo "  dbPath: /data" >> mongod.conf
    echo "  journal:" >> mongod.conf
    echo "    enabled: true" >> mongod.conf
    echo "" >> mongod.conf
    echo "processManagement:" >> mongod.conf
    echo "  fork: true" >> mongod.conf
    echo "  pidFilePath: /var/run/mongod/mongod.pid" >> mongod.conf
    
    if [ "${CONFIG_CLUSTER}" == "true" ]; then
	exit 0;
    fi
    
    #################################################################
    #  Enable munin plugins for iostat and iostat_ios
    #################################################################
    ln -s /usr/share/munin/plugins/iostat /etc/munin/plugins/iostat
    ln -s /usr/share/munin/plugins/iostat_ios /etc/munin/plugins/iostat_ios
    touch /var/lib/munin/plugin-state/iostat-ios.state
    chown munin:munin /var/lib/munin/plugin-state/iostat-ios.state
    
    #################################################################
    #  Figure out how much RAM we have and how to slice it up
    #################################################################
    memory=$(vmstat -s | grep "total memory" | sed -e 's/ total.*//g' | sed -e 's/[ ]//g' | tr -d '\n')
    if [ "${MICROSHARDS}" != "0" ]; then
        memory=$(printf %.0f $(echo "${memory} / 1024 / ${MICROSHARDS} * .9 / 1024" | bc))
    else
        memory=$(printf %.0f $(echo "${memory} / 1024 / 1 * .9 / 1024" | bc))
    fi
    
    if [ ${memory} -lt 1 ]; then
        memory=1
    fi
    #################################################################
    #  Make data directories and add symbolic links for journal files
    #################################################################
    
    
    #  Handle case when core sharding count is 0 - Karthik
    if [ "${MICROSHARDS}" != "0" ]; then
        c=0
        while [ $c -lt $MICROSHARDS ]
        do
            mkdir -p /data/${SHARD}-rs${c}
            mkdir -p /journal/${SHARD}-rs${c}
            
            # Add links for journal to data directory
            ln -s /journal/${SHARD}-rs${c} /data/${SHARD}-rs${c}/journal
            (( c++ ))
        done
    else
        mkdir -p /data/
        mkdir -p /journal/
        
        # Add links for journal to data directory
        ln -s /journal/ /data/journal
    fi
    
    mkdir -p /log
    mount /log
    
    #################################################################
    # Change permissions to the directories
    #################################################################
    chown -R mongod:mongod /journal
    chown -R mongod:mongod /log
    chown -R mongod:mongod /data
    
    #################################################################
    # Clone the mongod config file and create cgroups for mongod
    #################################################################
    c=0
    
    #  Handle case when core sharding count is 0 - Karthik
    if [ "${MICROSHARDS}" != "0" ]; then
        echo "" > /etc/cgconfig.conf
        while [ $c -lt $MICROSHARDS ]
        do
            cp mongod.conf /etc/mongod${c}.conf
            (( port++ ))
            
            #Line below overwrites the conf (fix reported by rick)
            #cp /etc/mongod.conf /etc/mongod${c}.conf
            sed -i "s/.*path:.*/  path: \/log\/mongod${c}.log/g" /etc/mongod${c}.conf
            sed -i "s/.*port:.*/  port: ${port}/g" /etc/mongod${c}.conf
            sed -i "s/.*dbPath:.*/  dbPath: \\/data\\/${SHARD}-rs${c}/g" /etc/mongod${c}.conf
            sed -i "s/.*pidFilePath:.*/  pidFilePath: \/var\/run\/mongod\/mongod${c}.pid/g" /etc/mongod${c}.conf
            echo "replication:" >> /etc/mongod${c}.conf
            echo "  replSetName: ${SHARD}-rs${c}" >> /etc/mongod${c}.conf
            
            cp /etc/init.d/mongod /etc/init.d/mongod${c}
            sed -i "s/CONFIGFILE=.*/CONFIGFILE=\"\/etc\/mongod${c}\.conf\"/g" /etc/init.d/mongod${c}
            sed -i "s/SYSCONFIG=.*/SYSCONFIG=\"\/etc\/sysconfig\/mongod${c}\"/g" /etc/init.d/mongod${c}
            
            echo "mount {
                cpuset  = /cgroup/cpuset;
                cpu     = /cgroup/cpu;
                cpuacct = /cgroup/cpuacct;
                memory  = /cgroup/memory;
                devices = /cgroup/devices;
              }

              group mongod${c} {
                perm {
                  admin {
                    uid = mongod;
                    gid = mongod;
                  }
                  task {
                    uid = mongod;
                    gid = mongod;
                  }
                }
                memory {
                  memory.limit_in_bytes = ${memory}G;
                  }
              }" >> /etc/cgconfig.conf
            
            
            echo CGROUP_DAEMON="memory:mongod${c}" > /etc/sysconfig/mongod${c}
            
            (( c++ ))
        done
    else #Karthik
        cp mongod.conf /etc/mongod.conf
        sed -i "s/.*port:.*/  port: ${port}/g" /etc/mongod.conf
        echo "replication:" >> /etc/mongod.conf
        echo "  replSetName: ${SHARD}-rs0" >> /etc/mongod.conf
        
        echo CGROUP_DAEMON="memory:mongod" > /etc/sysconfig/mongod
        
        echo "mount {
            cpuset  = /cgroup/cpuset;
            cpu     = /cgroup/cpu;
            cpuacct = /cgroup/cpuacct;
            memory  = /cgroup/memory;
            devices = /cgroup/devices;
          }

          group mongod {
            perm {
              admin {
                uid = mongod;
                gid = mongod;
              }
              task {
                uid = mongod;
                gid = mongod;
              }
            }
            memory {
              memory.limit_in_bytes = ${memory}G;
              }
          }" > /etc/cgconfig.conf
        
    fi
    
    
    #################################################################
    #  Start cgconfig, munin-node, and all mongod processes
    #################################################################
    chkconfig cgconfig on
    service cgconfig start
    
    chkconfig munin-node on
    service munin-node start
    
    #  Handle case when core sharding count is 0 - Karthik
    if [ "${MICROSHARDS}" != "0" ]; then
        c=0
        while [ $c -lt $MICROSHARDS ]
        do
            chkconfig mongod${c} on
            enable_all_listen
            service mongod${c} start
            (( c++ ))
        done
    else
        chkconfig mongod on
        enable_all_listen
        service mongod start
    fi
    
    #################################################################
    #  Primaries initiate replica sets
    #################################################################
    if [[ "$NODE_TYPE" == "Primary" ]]; then
	## we need to initialize our replica set so others can join
	## we need to wait a bit for the mongod process to calm down
	sleep 20
	conf="rs.initiate({_id: '${SHARD}-rs0', version: 1, members: [{ _id: 0, host : '${MY_IPADDRESS}:${port}' }]})"
        mongo --host ${MY_IPADDRESS} --port ${port} << EOF
$conf
EOF
	
        if [ ${SHARDCOUNT} -gt 0 ]; then
            #################################################################
            # Let the replica sets initialize
            #################################################################
            sleep 20
            
            #################################################################
            # Make sure the config servers are up
            #################################################################
            ./orchestrator.sh -w "WORKING=3" -n "CONFIG_${UNIQUE_NAME}"
            CONFIGADDRS=$(./orchestrator.sh -g -n "CONFIG_${UNIQUE_NAME}")
            read -a CONFIGADDRS <<< $CONFIGADDRS
            
            for addr in "${CONFIGADDRS[@]}"
            do
                while [ true ]; do
                    mongo --host ${addr} --port 27030 << EOF
use admin
EOF
                    if [ $? -eq 0 ]; then
                        break
                    fi
                    sleep 5
                done
            done
            
            #################################################################
            #Setup a mongos service
            #################################################################
            cp mongod.conf /etc/mongos.conf
            sed -i 's/.*path:.*/  path: \/log\/mongos.log/g' /etc/mongos.conf
            sed -i 's/.*port:.*/  port: 27017/g' /etc/mongos.conf
            sed -i 's/.*dbPath:.*/#/g' /etc/mongos.conf
            sed -i 's/.*storage:.*/#/g' /etc/mongos.conf
            sed -i 's/.*journal:.*/#/g' /etc/mongos.conf
            sed -i 's/.*enabled:.*/#/g' /etc/mongos.conf
            sed -i 's/.*pidFilePath:.*/  pidFilePath: \/var\/run\/mongod\/mongos.pid/g' /etc/mongos.conf
            echo "" >> /etc/mongos.conf
            echo "sharding: " >> /etc/mongos.conf
            echo "  configDB: ${CONFIGADDRS[0]}:27030,${CONFIGADDRS[1]}:27030,${CONFIGADDRS[2]}:27030" >> /etc/mongos.conf
            
            cp /etc/init.d/mongod /etc/init.d/mongos
            sed -i 's/CONFIGFILE=.*/CONFIGFILE="\/etc\/mongos\.conf"/g' /etc/init.d/mongos
            sed -i 's/mongod=.*/mongod="\/usr\/bin\/mongos"/g' /etc/init.d/mongos
            
            #################################################################
            # Launch mongos and add the shards
            #################################################################
            chkconfig mongos on
            enable_all_listen
            service mongos start
            
            c=0
            port=27018
            while [ ${c} -lt ${MICROSHARDS} ]
            do
                
                mongo << EOF
sh.addShard("${SHARD}-rs${c}/${MY_IPADDRESS}:${port}")
EOF
                
                if [ $? -ne 0 ]; then
                    echo "Error"
                    exit 1
                fi
                
                (( c++ ))
                (( port++ ))
            done
        fi
    fi
)
