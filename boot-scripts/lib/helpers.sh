#!/bin/bash

enable_all_listen() {
    for f in /etc/mongo*.conf
    do
        sed -e '/bindIp/s/^/#/g' -i ${f}
        sed -e '/bind_ip/s/^/#/g' -i ${f}
        echo " Set listen to all interfaces : ${f}"
    done
}
