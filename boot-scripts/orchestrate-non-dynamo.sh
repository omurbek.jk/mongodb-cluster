#!/bin/bash
set -x

error(){
    local error=${1}
    echo "$error"
    exit 1
}


## this gets the actual index of the member array, not it's _id
getReplicationIDsForHostPort(){

    local host_port=$1
    output="$(mongo --quiet <<EOF
JSON.stringify(rs.config())
EOF
)"

    myId="$(echo $output | jq ".members[] | select(.host==\"${host_port}\") | ._id")"
    index=0
    while read id; do
	if [ "$id" = "$myId" ]; then
            break
	fi
	index=$((index + 1))
    done <<< "$(echo $output | jq ".members[]._id")"
    echo $index

}

getLargestPriorityNumber(){

    local host_port=$1
    output="$(mongo --quiet <<EOF
JSON.stringify(rs.config())
EOF
)"
    
    priority="$(echo $output | jq ".members[].priority" | sort -n | tail -1)"
    echo $priority
}

getReplicationIndexes(){
    local host_port=$1
    host="$(echo $host_port | awk -F':' '{print $1}')"
    port="$(echo $host_port | awk -F':' '{print $2}')"
    status="$(mongo --host "${host}" --port "${port}" <<EOF
rs.status()
EOF
)"
    ids=""
    while read id; do
        i="$(echo $id | awk -F':' '{print $2}')"
        ids="${ids} ${i%,}"
    done <<<  "$(echo "$status" | grep '"_id" :')"
    echo "$ids"
}

checkIfHostPortStringIsPrimary(){
    local host_port=$1
    host="$(echo $host_port | awk -F':' '{print $1}')"
    port="$(echo $host_port | awk -F':' '{print $2}')"
    host_port_hostname="ip-$(echo ${host_port} | sed 's/\./-/g')"
    output="$(mongo --host "${host}" --port "${port}" --quiet <<EOF
JSON.stringify(rs.status())
EOF
)"  
    status="$(echo $output | jq ".members[] | select(.name==\"${host_port_hostname}\" and .stateStr==\"PRIMARY\")" | grep $host_port_hostname)"
    if [ -n "${status:-}" ]; then
        echo "0"
    else
        echo "1"
    fi
}

checkHostPortString(){
    local host_port=$1
    host="$(echo $host_port | awk -F':' '{print $1}')"
    port="$(echo $host_port | awk -F':' '{print $2}')"
    ret="$(checkHostPort $host $port)"
    echo $ret
}

checkHostPort(){
    local host=$1
    local port=$2

    output="$(nc -z -v -w5 $host $port)"
    output_code=$?
    echo $output_code
}

waitForHostPort(){
    local host_port="$1"
    if ! echo "$host_port" | grep -q ":"; then
        error "You must specify a host:port combination"
    fi
    host="$(echo $host_port | awk -F':' '{print $1}')"
    port="$(echo $host_port | awk -F':' '{print $2}')"
    while [ $(checkHostPort $host $port) -ne 0 ]; do
        sleep 5
    done
}

getUnhealthyMembers(){
    output="$(mongo --quiet <<EOF
JSON.stringify(rs.status())
EOF
)"
    names="$(echo $output | jq ".members[] | select(.health==0) | .name")"
    echo $names
}


usage() {
    cat <<EOF
    Usage: $0 [options]
        -h print usage
        -w wait for tcp <host>:<port> availability 
        -c check if "<host>:<port>" is open and listning
        -p check if "<host>:<port>" is the PRIMARY in the replication set
        -x get the replication member index for the "<host>:<port>" specified
        -i get all of the available _id's for the replication members
EOF
#    exit 0
}



# ------------------------------------------------------------------
#          Read all inputs
# ------------------------------------------------------------------
[[ $# -eq 0 ]] && usage;

while getopts "hux:i:p:c:w:r:" o; do
    case "${o}" in
	h) usage && exit 0
	    ;;
	w) waitForHostPort ${OPTARG}
	    ;;
	c) checkHostPortString ${OPTARG}
	    ;;
	p) checkIfHostPortStringIsPrimary ${OPTARG}
	    ;;
	i) getReplicationIndexes ${OPTARG}
	    ;;
	x) getReplicationIDsForHostPort ${OPTARG}
	    ;;
	r) getLargestPriorityNumber ${OPTARG}
	    ;;
	u) getUnhealthyMembers
	    ;;
  esac
done
