#!/bin/bash
set -x

source /etc/profile.d/cluster
port=27017

echo $(./orchestrate-non-dynamo.sh -u) | while read unhealthy_member; do
    ## double quotes are part of the return
    mongo --host "${PRIMARY_ADDRESS}" --port "${port}" <<EOF
rs.remove(${unhealthy_member})
EOF
done
