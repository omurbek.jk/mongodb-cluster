#!/bin/bash
set -x

cp ./orchestrate-non-dynamo.sh /home/ec2-user/mongodb/
chmod +x /home/ec2-user/mongodb/orchestrate-non-dynamo.sh
cp ./orchestrator.sh /home/ec2-user/mongodb/
chmod +x /home/ec2-user/mongodb/orchestrator.sh
