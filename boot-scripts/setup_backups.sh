#!/bin/bash
######################################################################
##
## Variables in this file:
##   - BACKUP_BUCKET
##   - BACKUP_BUCKET_REGION
##   - ENV
##   - MONGO_NAME
##   - MONGO_BACKUP_CRON

set -eux

pip install filechunkio

if ! rpm -qa | grep -q cloudcoreo-directory-backup; then
    yum install -y cloudcoreo-directory-backup
fi

MY_AZ="$(curl -sL 169.254.169.254/latest/meta-data/placement/availability-zone)"
MY_REGION="$(echo ${MY_AZ%?})"
backup_cron="${MONGO_BACKUP_CRON:-0 * * * *}"
backup_bucket_region="${BACKUP_BUCKET_REGION:-us-east-1}"
backup_dump_dir="/opt/backups"

workdir="$(pwd)"

## lets set up pre and post restore scripts
script_dir="/opt/cloudcoreo-directory-backup-scripts"
mkdir -p "$script_dir"
mkdir -p "$backup_dump_dir"
cat <<"EOF" > "${script_dir}/should-run-backup.sh"
#!/bin/bash
## this needs to only run if we are the ONLY server or if we are a SECONDARY
## if we are a SECONDARY, we need to run if we are the NEWEST SECONDARY
source /etc/profile.d/cluster
port=27017

status="$(mongo --quiet <<EOFI
JSON.stringify(rs.status())
EOFI
)"

numNodes="$(echo $status | jq '.members[]._id' | wc -l)"
numPrimaries="$(echo $status | jq '.members[] | select(.stateStr=="PRIMARY") | ._id' | wc -l)"

## if there is only one node and its the primary, we need to run the backups anyway
if [ $numNodes -ne $numPrimaries ]; then
    secondaryIds="$(echo $status | jq '.members[] | select(.stateStr=="SECONDARY") | ._id')"
    backupId="$(echo $status | jq '.members[] | select(.stateStr=="SECONDARY") | ._id' | tail -1)"
    myId="$(echo $status | jq ".members[] | select(.name==\"${MY_IPADDRESS}:${port}\") | ._id")"
    if [ "$myId" != "$backupId" ]; then
        ## we are not the backup node - exiting
        echo "we are not the backup node - exiting"
        exit 1
    fi
fi
## if we make it here, run the backup!
echo "running the backup"
exit 0
EOF
chmod +x "${script_dir}/should-run-backup.sh"

cat <<EOF > "${script_dir}/pre-backup.sh"
#!/bin/bash
."${script_dir}/should-run-backup.sh"
if [ $? -ne 0 ]; then
    echo "dont run the backup"
    exit 1
fi
mongodump --out ${backup_dump_dir}

EOF
cat <<EOF > "${script_dir}/post-backup.sh"
#!/bin/bash
rm -rf ${backup_dump_dir}/*

EOF
cat <<EOF > "${script_dir}/pre-restore.sh"
#!/bin/bash

EOF
cat <<EOF > "${script_dir}/post-restore.sh"
#!/bin/bash
set -eux
mongorestore ${backup_dump_dir}

EOF

S3_PREFIX="${MY_REGION}/mongo/${ENV}/${SHARD}"
## now we need to perform the restore
(
    cd /opt/; 
    python cloudcoreo-directory-backup.py --s3-backup-region ${backup_bucket_region} --s3-backup-bucket ${BACKUP_BUCKET} --s3-prefix "${S3_PREFIX}" --directory ${backup_dump_dir} --dump-dir /tmp --restore --post-restore-script "${script_dir}/post-restore.sh" --pre-restore-script "${script_dir}/pre-restore.sh" || true
)

## now that we are restored, lets set up the backups
if ! cat /etc/crontab | grep -q cloudcoreo-directory-backup; then
    echo "${backup_cron} ps -fwwC python | grep -q cloudcoreo-directory-backup || { cd /opt/; mkdir -p ${backup_dump_dir}; sh ${script_dir}/should-run-backup.sh && nohup python cloudcoreo-directory-backup.py --s3-backup-region ${backup_bucket_region} --s3-backup-bucket ${BACKUP_BUCKET} --s3-prefix $S3_PREFIX --directory ${backup_dump_dir} --dump-dir /tmp --pre-backup-script ${script_dir}/pre-backup.sh --post-backup-script ${script_dir}/post-backup.sh & }" >> /etc/crontab
    cat /etc/crontab | crontab
fi
