#!/bin/bash
set -x

#################################################################
# Make filesystems, set ulimits and block read ahead on ALL nodes
#################################################################
mkfs -t ext4 /dev/xvdf
echo "/dev/xvdf /data ext4 defaults,auto,noatime,noexec 0 0" | tee -a /etc/fstab
mkdir -p /data
mount /data
chown -R mongod:mongod /data
blockdev --setra 32 /dev/xvdf
rm -rf /etc/udev/rules.d/85-ebs.rules
touch /etc/udev/rules.d/85-ebs.rules
echo 'ACTION=="add", KERNEL=="'$1'", ATTR{bdi/read_ahead_kb}="16"' | tee -a /etc/udev/rules.d/85-ebs.rules
echo "* soft nofile 64000
* hard nofile 64000
* soft nproc 32000
* hard nproc 32000" > /etc/limits.conf


## if this is a config server, we are done...
if [ "${CONFIG_CLUSTER}" == "true" ]; then
    exit 0;
fi

## if we make it here, we are a data node
#################################################################
# Make the filesystems, add persistent mounts
#################################################################
mkfs -t ext4 /dev/xvdg
mkfs -t ext4 /dev/xvdh

echo "/dev/xvdg /journal ext4 defaults,auto,noatime,noexec 0 0" | tee -a /etc/fstab
echo "/dev/xvdh /log ext4 defaults,auto,noatime,noexec 0 0" | tee -a /etc/fstab

#################################################################
# Make directories for data, journal, and logs
#################################################################
mkdir -p /journal
mount /journal
        
