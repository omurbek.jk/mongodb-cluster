#!/bin/bash

mkdir -p /home/ec2-user/mongodb
(
    echo export TABLE_NAMETAG=${CLUSTER_NAME}  >> config.sh
    echo export MongoDBVersion=${MONGO_VERSION}  >> config.sh
    echo export VPC=${VPC_NAME}  >> config.sh
    chown -R ec2-user: /home/ec2-user/
)
