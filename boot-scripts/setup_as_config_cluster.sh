#!/bin/bash
set -x
source lib/helpers.sh

if [ ! "${CONFIG_CLUSTER}" == "true" ]; then
    echo "skipping config setup because variable CONFIG_CLUSTER != 'true'"
    exit 0;
fi

#################################################################
# Assign CONFIGINDEX if config cluster
#################################################################

if [ "${CONFIG_CLUSTER}" == "true" ]; then
    NODE_TYPE="Config"
    if [ "${IS_LEADER}" == "true" ]; then
	CONFIGINDEX=0
    else
	CONFIGINDEX=1
    fi
fi
(
    workdir="$PWD"
    cd /home/ec2-user/mongodb
    
    if [ "${CONFIGINDEX}" == "0" ]; then
	./orchestrator.sh -c -n "CONFIG_${UNIQUE_NAME}"
	./orchestrator.sh -s "WORKING" -n "CONFIG_${UNIQUE_NAME}"
    else [ "${CONFIGINDEX}" == "1" ] || [ "${CONFIGINDEX}" == "2" ]; then
	./orchestrator.sh -b -n "CONFIG_${UNIQUE_NAME}"
	./orchestrator.sh -w "WORKING=1" -n "CONFIG_${UNIQUE_NAME}"
	./orchestrator.sh -s "WORKING" -n "CONFIG_${UNIQUE_NAME}"
    fi
    
    #################################################################
    # Modify the mongod.conf file and make this a config server
    #################################################################
    cp mongod.conf /etc/mongod.conf
    sed -i 's/.*path:.*/  path: \/data\/mongod.log/g' /etc/mongod.conf
    sed -i 's/.*port:.*/  port: 27030/g' /etc/mongod.conf
    sed -i '#s/.*dbPath:.*/  dbpath: \/data/g' /etc/mongod.conf
    sed -i 's/# location.*/configsvr=true/g' /etc/mongod.conf
    echo "" >> /etc/mongod.conf
    echo "sharding:" >> /etc/mongod.conf
    echo "  clusterRole: configsvr" >> /etc/mongod.conf
    
    mkdir -p /data/configdb/
    chown mongod:mongod /data/configdb/
    enable_all_listen
    
    chkconfig mongod on
    service mongod start
)
